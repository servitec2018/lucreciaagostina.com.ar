=== WooCommerce Countdown Timer,Enquiry Form , Exit Popup – WP Sales Booster ===
Contributors: footnoteio, ruhel241
Tags:  Remove add to cart button woocommerce, woocommerce disable cart and checkout, woocommerce hide price, countdown timer
Requires at least: 4.5
Requires PHP: 5.4 or greater
Tested up to: 5.3.0
Stable tag: 1.6.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Supercharge your WordPress WooCommerce site with showing countdown timer for discount.

== Description ==

WP Sales Booster is  with most amazing salas increasing plugin with WooCommerce Countdown Timer [Which creates scarcity to visitor and influence to purchase  ] .Exit Popup [ WooCommerce  administrator can collect potential customer leads [email address and phone number  ] and also can re-target the customer using auto responder  [ different email marketing tools ]. Another great feature is Enquiry Form [Webmaster can set custom Enquiry Form for each product  to make instant email communication with potential customer ] . This plugin is focused on WooCommerce and we are a team of people added many proven  features to boost your WooCommerce sales and conversion with this plugin.

[youtube https://www.youtube.com/watch?v=z5LngNnhGLA]

### Remove Add to Cart Button Woocommerce
Sometimes Woocommerce website owner needs to remove the add to cart button for any specific product . This plugin allow this option to user .any one can easily remove add to cart button even without having any coding skills.

### Woocommerce Hide Price
WooCommerce website owner a hide price for any specific product . This plugin allow users to hide price for any specific product.

### Sticky Header Note WooCommerce
This features allow to grab the attention of your user by releasing special note with the countdown timer . As an example, a special note can be Next Day Delivery, Free Shipping, Discount Code etc. which will be displayed to your product page header.

### Woocommerce Order Notes
Woocommerce website owner can release special note for individual products below cart option here you can place your discount coupon for any specific promotion offer  with timer.

### User Based Countdown Timer
Website owner can set  unique timer for individual user. This is the most advanced feature and it's a proven method of increasing sales. By this feature you can set special note to any customer for a specific timer for 0 to 120 minutes. If audience not grab the offer note or discountcount code. Note will be vanish for the user for the user for a specific minutes. This is really a great trigger to increase your sales.

### Fixed Date Countdown Timer
WP Sales Booster allows clear deadline to your shoppers. So  a website owner can  create a campaign targeting a specific deadline .Any one can set a campaign with coupon or special note with this plugin for a special occasion. Such as Black friday, Critsmash, cyber monday

### WooCommerce Show Stock Quantity
WooCommerce website owner also can give extra kick with this feature to their customers. you can show your inventory status to your customer by showing this  you can also create scarcity to your customer.

### Change Colours Of Each Features  of Countdown Timer
Topbar Primary Background Color ,Topbar Secondary Background Color ,Top Bar Text Color ,Countdown Background Color, Countdown Timer Color ,Countdown Text Color , Stock Quantity Color to grab the attention of your audience

### Set Product Enquiry Button For Particular Product
Website admin can set custom product product enquiry form for each product  . With auto massage system and also with custom email

### Drag Drop Custom Enquiry Form
Web admin can set custom form with name phone number or other fields easily by integrating fluent form (free version ) . also can use set of awesome free templates

### Enquiry Form Conversion Report
We admin also can track the number of views of each form and also can track how the form is converting .

### Custom Thank You Text , Redirect To Another Page Or Custom Page
Web admin can set custom thank you text , custom page or custom url for user . So when any customer pressed the submit button he will be redirected to specific text or page

### Custom Auto Massage Option For Each Inquiry Form
Web admin can set individual message for each form  . So user will get individual auto massage massage for individual product for each query !

### Change The Enquiry Button Text
Webmaster can set the enquiry button text with his that he or she wants

### Responsive Ready
All forms  and timer is are responsive on electronic devices

### Custom Exit Popup Form
Collect subscriber by showing custom exit pop up form or note for individual product . User can create custom form with drag drop features and also can show third party forms by mailchimp aweber getresponse etc .

### Banner and corner Ad
Webmaster  can set banner and corner image for campaign setting and also for driving traffic to any specific product or landing page !


== Installation ==

This section describes how to install the plugin and get it working.
e.g.

1. Upload the plugin files to the /wp-content/plugins/ directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the ‘Plugins’ screen in WordPress

== Frequently Asked Questions ==

= Does it really boost sales =
Yes it increases sales 2 to 6 times based on your product and niche. All features of this countdown timer plugin is optimized for increasing sales.

= Does this plugin regularly updates =
Yes , we people behind this plugin regularly working to improve this plugin and also the features based on customer experience .

= How Do You Provide Support =
We people behind this plugin provide support through WordPress forums and for our pro version users we provide live support and also email support 24/7.

= Why Woocommerce Website owner should use this plugin =
This is the most advanced and effective plugging in the market to increase sales to any woocommerce website on the other hand it allows different features  by which a woocommerce website owner care create different types of campaigns and promotion to speed up  you their sales

= What is difference  with user based timer and fixed date timer? =
By setting user based timer for woocommerce website anyone can create urgency for 10 to 120 minutes and on the other hand by using fixed date timer you can set the timer for any special events or date such as black friday cyber monday etc .

= Can I remove add to cart button for any specific product? =
Yes, you can remove add to cart button without having coding skill.

= Can I hide price for any specific product. =
Yes, Anyone can hide price without having coding skill too.

= Can I set timer for specific product =
This  plugin is designed for specific products so you can use all the features of this plugin for specific products .

= Can I set timer for individual customer =
Yes, you can set custom timer for individual users

= I want to set countdown timer as 30 minutes for individual user does it possible =
Yes you can, you can use user based timer feature for doing this task. you can set 0 to 120 minutes timer for your individual users.

= Can I set enquiry page for specific product =
Yes, You can set enquiry page for certain products with this plugin.

= Can I set custom contact page for custom product =
Yes, you can set custom contact page for custom product

= Can I remove add to cart button with this plugin =
Yes, you can easily remove add to cart button with this plugin

= Can I show custom coupon with this timer =
Yes, you can set custom coupon code with this timer ,when the timer hits zero your coupon code or note will be disappeared

= Can I show custom message in header with this timer =
Yes, you can show custom message to your customer for any specific product in sticky header.

= Can I run specific date based campaign with this timer =
Yes, You can run specific date or event based countdown timer with this plugin in your ecommerce website.

= Can I change the woocommerce button with this timer =
Yes, you can change woocommerce button text with this plugin


== Screenshots ==
1. Woo Sales Booster Settings
2. Show timer on frontend
3. Hide Price Option on Frontend
4. Enquire Us Modal View


== Changelog ==

= 1.6.0 =
* Improve many areas
* Added new features

= 1.5.0 =
* Fixed few bug for timer
* Improved styles
* Improved Admin panel Settings
* Improved Overall Performence

= 1.0 =
* Init First version

== Upgrade Notice ==