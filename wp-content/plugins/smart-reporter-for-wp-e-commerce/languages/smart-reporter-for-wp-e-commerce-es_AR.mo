��          �            h  2   i  T  �  I   �  ]   ;  u   �       .     (   M  l   v  ,   �       	   .     8  1   S  }  �  4     �  8  O   	  ^   b	  q   �	     3
  )   C
  1   m
  �   �
  5   #     Y  	   w     �  1   �                                 
      	                                         <span class="sr_pricing_icon"> 🔥 </span> Go Pro <strong>Common features in every plan</strong>: 153+ metrics and reports, automatic consolidation & deduping, growth advice from experts, website & visitor analytics, team access, real time transaction data, sales, products and customer analytics, in-depth customer profiles, weekly email digests, goal tracking & forecasting and much more! <strong>Pro Version Installed.</strong> Store analysis like never before. Accurate metrics, real time reports and actionable insights that grow your WooCommerce store. And we believe dollar to dollar – <strong>Putler is the best investment for the future of your businesses</strong>. Explore Putler Get Advanced WooCommerce Analytics & Reporting People call it priceless, yet affordable Putler <u>does the work of an employee</u> – taking care of reporting, analysis and spotting growth ideas. SMART REPORTER PRO IS NOW MERGED INTO PUTLER Smart Reporter For E-commerce StoreApps https://www.storeapps.org/ https://www.storeapps.org/product/smart-reporter/ Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-09-01 22:27+0000
PO-Revision-Date: 2019-09-01 22:27+0000
Last-Translator: 
Language-Team: Español de Argentina
Language: es_AR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ , <span class="sr_pricing_icon"> 🔥 </span> Go Pro <strong>características Comunes en cada plan de</strong>: 153+ métricas y reportes, consolidación automática & deduping, el crecimiento de los consejos de los expertos, página web y análisis de los visitantes, el equipo de acceso, en tiempo real de los datos de la transacción, ventas de productos y analisis de clientes, en la profundidad de los perfiles de los clientes, correo electrónico semanal de resúmenes, la meta de seguimiento y pronóstico y mucho más! <strong>Versión Pro Instalado.</strong> Tienda de análisis como nunca before. Mediciones precisas, informes en tiempo real y datos útiles que crecer tu tienda WooCommerce. Y creemos que el dólar dólar – <strong>Putler es la mejor inversión para el futuro de sus negocios</strong>. Explorar Putler Avanzado WooCommerce Análisis E Informes La gente la llama no tiene precio, pero asequible Putler <u>hace el trabajo de un empleado</u> – cuidar de la presentación de informes, análisis y manchado de crecimiento ideas. SMART REPORTERO PRO ES QUE AHORA SE HA UNIDO A PUTLER Smart Reportero De E-commerce StoreApps https://www.storeapps.org/ https://www.storeapps.org/product/smart-reporter/ 