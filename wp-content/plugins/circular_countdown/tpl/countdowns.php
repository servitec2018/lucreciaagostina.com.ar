<div class="wrap">
	<div id="lbg_logo">
			<h2><?php esc_html_e( 'Manage CountDowns' , 'circular-countdown' );?></h2>
 	</div>
    <div><p><?php esc_html_e( 'From this section you can add multiple countdowns.' , 'circular-countdown' );?></p>
    </div>

<div id="previewDialog"><iframe id="previewDialogIframe" src="" width="100%" height="600" style="border:0;"></iframe></div>

<div style="text-align:center; padding:0px 0px 20px 0px;"><img src="<?php echo plugins_url('images/icons/add_icon.gif', dirname(__FILE__))?>" alt="add" align="absmiddle" /> <a href="?page=circular_countdown_Add_New"><?php esc_html_e( 'Add new (CountDown)' , 'circular-countdown' );?></a></div>

<table width="100%" class="widefat">

			<thead>
				<tr>
					<th scope="col" width="5%"><?php esc_html_e( 'ID' , 'circular-countdown' );?></th>
					<th scope="col" width="26%"><?php esc_html_e( 'Name' , 'circular-countdown' );?></th>
					<th scope="col" width="26%"><?php esc_html_e( 'Shorcode' , 'circular-countdown' );?></th>
					<th scope="col" width="36%"><?php esc_html_e( 'Actions' , 'circular-countdown' );?></th>
					<th scope="col" width="7%"><?php esc_html_e( 'Preview' , 'circular-countdown' );?></th>
				</tr>
			</thead>

<tbody>
<?php foreach ( $result as $row )
	{
		$row=circular_countdown_unstrip_array($row); ?>
							<tr class="alternate author-self status-publish" valign="top">
					<td><?php echo esc_html($row['id'])?></td>
					<td><?php echo esc_html($row['name'])?></td>
					<td>[circular_countdown settings_id='<?php echo esc_html($row['id'])?>']</td>
					<td>
						<a href="?page=circular_countdown_Settings&amp;id=<?php echo esc_attr($row['id'])?>&amp;name=<?php echo esc_attr($row['name'])?>"><?php esc_html_e( 'CountDown Settings' , 'circular-countdown' );?></a> &nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="?page=circular_countdown_Playlist&amp;id=<?php echo esc_attr($row['id'])?>&amp;name=<?php echo esc_attr($row['name'])?>"><?php esc_html_e( 'Social Channels' , 'circular-countdown' );?></a> &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="?page=circular_countdown_Manage_CountDowns&id=<?php echo esc_attr($row['id'])?>" onclick="return confirm('Are you sure?')" style="color:#F00;">Delete</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="?page=circular_countdown_Manage_CountDowns&amp;duplicate_id=<?php echo esc_attr($row['id'])?>"><?php esc_html_e( 'Duplicate' , 'circular-countdown' );?></a></td>
					<td align="center"><a href="javascript: void(0);" onclick="showDialogPreview(<?php echo esc_js($row['id'])?>)"><img src="<?php echo plugins_url('images/icons/magnifier.png', dirname(__FILE__))?>" alt="preview" border="0" align="absmiddle" /></a></td>
	            </tr>
<?php } ?>
						</tbody>
		</table>





</div>
