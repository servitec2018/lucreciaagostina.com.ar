<div class="wrap">
		<div id="lbg_logo">
			<h2><?php esc_html_e( 'Overview' , 'circular-countdown' );?></h2>
		</div>
		<div class="postbox-container" style="width:100%">
			<div class="postbox">
				<h3 style="padding:7px 10px;"><?php esc_html_e( 'WordPress CountDown Pro - WebSites/Products Launch' , 'circular-countdown' );?></h3>
				<div class="inside">
				<p><?php esc_html_e( 'You can use it as Countdown for WebSites with Maintenance mode enabled, for Events and Products launch or as expiry date for Offers and Discounts' , 'circular-countdown' );?></p>
				<p><?php esc_html_e( 'You have available the following sections:' , 'circular-countdown' );?></p>
				<ul class="lbg_list-1">
					<li><a href="?page=circular_countdown_Manage_CountDowns"><?php esc_html_e( 'Manage CountDowns' , 'circular-countdown' );?></a></li>
					<li><a href="?page=circular_countdown_Add_New"><?php esc_html_e( 'Add New (CountDown)' , 'circular-countdown' );?></a></li>
		          <li><a href="?page=circular_countdown_Help"><?php esc_html_e( 'Help' , 'circular-countdown' );?></a></li>
				</ul>
			  </div>
			</div>
		</div>
	</div>
