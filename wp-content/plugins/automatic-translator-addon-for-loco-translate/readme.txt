﻿=== Loco Automatic Translate Addon ===
Contributors:narinder-singh,satindersingh,coolplugins
Tags:loco,translate,translation,translator,localization,language,translations,loco translate,loco addon
Requires at least:4.5
Tested up to:5.3
Requires PHP:5.6
Stable tag:trunk
License:GPLv2 or later
License URI:http://www.gnu.org/licenses/gpl-2.0.html

Automatic language translator add-on for Loco Translate plugin to translate WordPress plugins and themes translation files.

== Description ==

**Automatic Machine Translator Addon For Loco Translate**

Install this plugin along with the famous **[Loco Translate](https://wordpress.org/plugins/loco-translate/)** plugin (**1+ Million Active Installations**) and automatically machine translate any WordPress plugin or theme translation files into any language.

= Loco Addon Features: =
* One-click translate any plugin or theme all translatable strings.
* You can automatic translate upto **300,000 characters** daily free of cost (you can extend this limit via [premium license key](https://locotranslate.com/addon/loco-automatic-translate-premium-license-key/)).
* Check auto translations inside Loco build-in editor to manually edit any machine translated string.
* This plugin **[Powered by Yandex.Translate](http://translate.yandex.com/)** to auto translate plugin / theme language(po) files. Grab free api key:- [https://translate.yandex.com/developers/keys](https://translate.yandex.com/developers/keys)

> “If you spend too much time thinking about a thing, you'll never get it done. Stop wasting time, complete work smartly & quickly!”

= FREE v/s Premium License Key: =
* Free License - can automatic transalte **300,000 characters / day** & **1,000,000 characters / month**.
* Premium License - can automatic translate **1,000,000 characters** / day & **10,000,000 characters / month**.
* Free License - can only automatic translate **plain text strings**.
* Premium License - can also automatic translate **strings with HTML**.
* Free License Users - can ask for support only via WordPress Support Forum (**Support time:- 7-10 days**).
* Premium License Users - will receive quick support via email - contact@coolplugins.net (**Support time:- 24-48 hours**).
* Buy Premium License - **[$9 / Half Year](https://locotranslate.com/addon/loco-automatic-translate-premium-license-key/)** or **[$15 / Year](https://locotranslate.com/addon/loco-automatic-translate-premium-license-key/)**

> “Many people make the mistake of saving money by wasting time.”

= Who's Behind: =

This plugin is not developed by or affiliated with the "**Loco Translate**". It is a third party addon that provides automatic machine translations to quickly translate your theme or plugin language files.

We(**CoolPlugins.net**) only manage [LocoTranslate.com](https://locotranslate.com) website, Loco Automatic Translate Addon(**this plugin**) and [Loco Automatic Translate Addon Premium License Key](https://locotranslate.com/addon/loco-automatic-translate-premium-license-key/).

> We provide cool solutions to remove famous plugins limitations!

= Special THANKS!: =
Special thanks to famous **[Loco Translate](https://wordpress.org/plugins/loco-translate/)** plugin authors for creating an awesome plugin for translations and Yendex for providing translate API.

All automatic translations will machine translations, powered by Yendex Translate API, so we don't guarantee 100% correctness, please check all translated text carefully before making it live on your production site.
[](http://coderisk.com/wp/plugin/automatic-translator-addon-for-loco-translate/RIPS-FN0AdXlllg)
== Installation ==
1. Install **Loco Automatic Translate Addon** from the WordPress.org repository or by uploading plugin-zip unzipped folder to the **/wp-content/plugins** directory. You must also install **[Loco Translate](https://wordpress.org/plugins/loco-translate/)** free plugin if you want to use this addon.

2. Activate the plugin through **Plugins >> Installed Plugin** menu in WordPress

3. After plugin activation you need to insert Yendex API key inside **Loco Translate >> Loco Auto Translator**. You can grab a free api key at here:- [https://translate.yandex.com/developers/keys](https://translate.yandex.com/developers/keys)

4. While editing any plugin or theme language file using Loco build-in editor, you will find an auto translator button at top to quickly translate all translatable strings with one-click.

== Frequently Asked Questions ==
= How it works? =
This plugin works as an addon for **Loco Translate** plugin. First you need to install and activate free version of "Loco translate" then install this addon and use one-click machine translations (supported by Yendex API).

= Are you using any language translation API? =
**[Yendex Translate API](https://tech.yandex.com/translate/)**, you can read more about its terms to use at here - https://yandex.com/legal/translate_api/ Also grab a free API key at here:- [https://translate.yandex.com/developers/keys](https://translate.yandex.com/developers/keys)

= Is there any translation limit? =
For **free license** users:- The volume of the text translated: 300,000 characters per day but not more than 1,000,000 per month.

For **premium license** users:- The volume of the text translated: 1,000,000 characters per day but not more than 10,000,000 per month.

== Screenshots ==
1. Automatic Loco Translate Addon
2. Translations Using Yendex API
3. Free License v/s Premium License

== Changelog ==
<strong>Version 1.4.1 | 8 JAN 2020</strong>
<pre>
Fixed:Minor JS bug fixes
</pre>
<strong>Version 1.4 | 31 DEC 2019</strong>
<pre>
Added: Integrated HTML String Translation Feature
Added: Supported Norwegian and other missing languages
Added: Integrated Translation settings popup.
Imporved: Optimized code
Improved: Updated Preloader
Fixed: Unsaved string highlighting issue
Fixed: minor JS issues
Fixed: Wrong characters calculation bug

</pre>
<strong>Version 1.3.2 | 13 DEC 2019</strong>
<pre>
Added:Integrated URL and link filters in String
Added: Added string filters
Improved: JS code
Improved: feedback from
Improved: minor issues
</pre>
<strong>Version 1.3 | 03 DEC 2019</strong>
<pre>
Added: Integrated translated characters stats tables
Added: Extend characters limit with Premium License key
Added: Integrated namespace
Added: Not interested button in review popup
Added: Integrated Premium License key Manager 
Added: Added security checks in every request
Added: Integrated nonce in ajax request
Improved: Improved translation issues
Improved: Imporved overall code 
Improved: readme.text
Improved: Added new screenshots
Fixed: Minor translation issues.
Fixed: minor JS errors.
Fixed: compatibility issues with WordPress 5.3
</pre>
<strong>Version 1.2.1|13 Sep 2019</strong>
<pre>
Fixed: instant review popup notification bug fixed
</pre>
<strong>Version 1.2 | 02 Sep 2019</strong>
<pre>
Fixed: Mismatch translation strings in Turkish.
Fixed: minor js issues.
Imporved: minor textual changes
Added: Feedback on plugin deactivation.
</pre>
<strong>Version 1.1 | 31 Aug 2019</strong>
<pre>
Added: Integrated automatic translation progress bar popup
Added: Batch translation of all untranslated words in a single click.
Fixed: Issue with html translation.
Fixed: Minor javascript issues.
</pre>
<strong>Version 1.0.2 | 08 July 2019</strong>
<pre>
Fixed:Translations issues with Chinese language.
</pre>
<strong>Version 1.0 | 08 June 2019</strong>
<pre>
New: Initial Plugin Release
</pre>