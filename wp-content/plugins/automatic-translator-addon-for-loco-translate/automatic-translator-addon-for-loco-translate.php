<?php
/*
Plugin Name:Loco Automatic Translate Addon
Description:Auto language translator add-on for Loco Translate plugin to translate plugins and themes translation files into any language via fully automatic machine translations via Yendex Translate API.
Version:1.4.1
License:GPL2
Text Domain:atlt
Domain Path:languages
Author:Cool Plugins
Author URI:https://coolplugins.net/
 */
namespace LocoAutoTranslateAddon;
use  LocoAutoTranslateAddon\Helpers\Helpers;
 /**
 * @package Loco Automatic Translate Addon
 * @version 1.4.1
 */
if (!defined('ABSPATH')) {
    die('WordPress Environment Not Found!');
}

define('ATLT_FILE', __FILE__);
define('ATLT_URL', plugin_dir_url(ATLT_FILE));
define('ATLT_PATH', plugin_dir_path(ATLT_FILE));
define('ATLT_VERSION', '1.4.1');

class LocoAutoTranslate
{
    public function __construct()
    {
        register_activation_hook( ATLT_FILE, array( $this, 'atlt_activate' ) );
        register_deactivation_hook( ATLT_FILE, array( $this, 'atlt_deactivate' ) );
        add_action('plugins_loaded', array($this, 'atlt_check_required_loco_plugin'));
            /*** Template Setting Page Link inside Plugins List */
        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this,'atlt_settings_page_link'));
        add_action( 'admin_enqueue_scripts', array( $this, 'atlt_enqueue_scripts') );
        add_action('wp_ajax_atlt_translation', array($this, 'atlt_translate_string_callback'), 100);
        add_action('init',array($this,'checkStatus'));
        add_action('plugins_loaded', array($this,'include_files'));
        add_action('testRequest',array($this,'testHtmltranslation'));
    }

    /**
     * create 'settings' link in plugins page
     */
    public function atlt_settings_page_link($links){
        $links[] = '<a style="font-weight:bold" href="'. esc_url( get_admin_url(null, 'admin.php?page=loco-atlt') ) .'">Settings</a>';
        return $links;
    }

   /*
   |----------------------------------------------------------------------
   | required php files
   |----------------------------------------------------------------------
   */
   public function include_files()
   {
        include_once ATLT_PATH . 'includes/Helpers/Helpers.php';
        include_once ATLT_PATH . 'includes/Core/class.settings-api.php';
        include_once ATLT_PATH . 'includes/Core/class.settings-panel.php';
        new Core\Settings_Panel();
         if ( is_admin() ) {
            include_once ATLT_PATH . "includes/ReviewNotice/class.review-notice.php";
            new ALTLReviewNotice\ALTLReviewNotice(); 
            include_once ATLT_PATH . 'includes/Feedback/class.feedback-form.php';
            new FeedbackForm\FeedbackForm();
                //require_once ATLT_PATH . "includes/init-api.php";  
            include_once ATLT_PATH . 'includes/Register/LocoAutomaticTranslateAddonPro.php';
            } 
   }
   public function checkStatus(){
         Helpers::checkPeriod();
   }
  
   /*
   |----------------------------------------------------------------------
   | check if required "Loco Translate" plugin is active
   | also register the plugin text domain
   |----------------------------------------------------------------------
   */
   public function atlt_check_required_loco_plugin()
   {

      if (!function_exists('loco_plugin_self')) {
         add_action('admin_notices', array($this, 'atlt_plugin_required_admin_notice'));
      }
      load_plugin_textdomain('atlt', false, basename(dirname(__FILE__)) . '/languages/');

   }

   /*
   |----------------------------------------------------------------------
   | Notice to 'Admin' if "Loco Translate" is not active
   |----------------------------------------------------------------------
   */
   public function atlt_plugin_required_admin_notice()
   {
      if (current_user_can('activate_plugins')) {
         $url = 'plugin-install.php?tab=plugin-information&plugin=loco-translate&TB_iframe=true';
         $title = "Loco Translate";
         $plugin_info = get_plugin_data(__FILE__, true, true);
         echo '<div class="error"><p>' . sprintf(__('In order to use <strong>%s</strong> plugin, please install and activate the latest version of <a href="%s" class="thickbox" title="%s">%s</a>', 'atlt'), $plugin_info['Name'], esc_url($url), esc_attr($title), esc_attr($title)) . '.</p></div>';
         deactivate_plugins(__FILE__);
      }
   }

   /*
   |------------------------------------------------------------------------
   |  Enqueue required JS file
   |------------------------------------------------------------------------
   */
   function atlt_enqueue_scripts(){
       
       wp_deregister_script('loco-js-editor');
       wp_register_script( 'loco-js-editor', ATLT_URL.'assets/js/loco-js-editor.min.js', array('loco-js-min-admin'),false, true);
       
       if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'file-edit') {
            $data=array();
            wp_enqueue_script('loco-js-editor');
            $status=Helpers::atltVerification();
            $nonce = wp_create_nonce('atlt_nonce');
            if($status['type']=="free"){
                $data['api_key']=Helpers::getAPIkey();
                $data['info']=Helpers::atltVerification();
                $data['nonce']=$nonce;
            }else{
                $data['api_key']=Helpers::getAPIkey();
                $key=Helpers::getLicenseKey();
                if(Helpers::validKey( $key)){
                    $data['info']['type']="pro";
                    $data['info']['allowed']="yes";
                    $data['info']['licenseKey']=$key;
                    $data['nonce']=$nonce;
                }
            }
            $extraData['preloader_path']=ATLT_URL.'/assets/images/preloader.gif';
            wp_localize_script('loco-js-editor', 'ATLT', $data);
            wp_localize_script('loco-js-editor', 'extradata', $extraData);
            
       }

   }

    /*
   |----------------------------------------------------------------------
   | AJAX called to this function for translation
   |----------------------------------------------------------------------
   */
  public function atlt_translate_string_callback()
  {
      // verify request
    if ( ! wp_verify_nonce($_REQUEST['nonce'], 'atlt_nonce' ) ) {
           die(json_encode(array('code' => 850, 'message' => 'Request Time Out. Please refresh your browser window.')));
       } else {
               // user status
           $status=Helpers::atltVerification();
           if($status['type']=="free" && $status['allowed']=="no"){
               die(json_encode(array('code' => 800, 'message' => 'You have consumed daily limit')));
           }

           // grab API keys
           $api_key_arr = Helpers::getAPIkey();
           $apiKey = $api_key_arr['atlt_api-key'];
           // get request vars
           if (empty($_REQUEST['data'])) {
               die(json_encode(array('code' => 900, 'message' => 'Empty request')));
           }  

       if(isset($_REQUEST['data'])){

           $requestData = $_REQUEST['data'];
           $targetLang=$_REQUEST['targetLan'];
           $sourceLang=$_REQUEST['sourceLan'];
           if($targetLang=="nb"){
               $targetLang="no";
           }
           $lang = $sourceLang.'-'.$targetLang;
           $request_chars  = $_REQUEST['requestChars'];
           $totalChars  = $_REQUEST['totalCharacters'];
           $requestType=$_REQUEST['strType'];   
         
      
       // create query string 
       $queryString='';
       $stringArr= json_decode(stripslashes($requestData),true);  
       if(is_array($stringArr)){
           foreach($stringArr as $str){
               $queryString.='&text='.urlencode($str);
           }
       }
       // build query
       $buildReqURL='';
       $buildReqURL.='https://translate.yandex.net/api/v1.5/tr.json/translate';
       $buildReqURL.='?key=' . $apiKey . '&lang=' . $lang.'&format='.$requestType;
       $buildReqURL.=$queryString;
       // get API response 
        $response = wp_remote_get($buildReqURL, array('timeout'=>'180'));
   
       if (is_wp_error($response)) {
           return $response; // Bail early
       }
       $body = wp_remote_retrieve_body($response);
       $data = json_decode( $body, true);   // convert string into assoc array
     
       if($data['code']==200){
           // grab translation count data
       $today_translated = Helpers::todayTranslated( $request_chars);
       $monthly_translated = Helpers::monthlyTranslated( $request_chars);
       /** Calculate the total time save on translation */
       $session_time_saved = Helpers::atlt_time_saved_on_translation( $totalChars);
       $total_time_saved = Helpers::atlt_time_saved_on_translation($totalChars);
      // create response array
       $data['stats']=array(
                       'todays_translation'=>$today_translated,
                       'total_translation'=>$monthly_translated,
                       'time_saved'=> $session_time_saved,
                       'total_time_saved'=>$total_time_saved,
                       'totalChars'=>$totalChars
                   );
       }  
       die(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

   }
  }

   /*
   |------------------------------------------------------
   |    Plugin activation
   |------------------------------------------------------
    */
   public function atlt_activate(){
       $plugin_info = get_plugin_data(__FILE__, true, true);
       update_option('atlt_version', $plugin_info['Version'] );
       update_option("atlt-installDate",date('Y-m-d h:i:s') );
       update_option("atlt-ratingDiv","no");
       update_option("atlt-type","free");
   }


   /*
   |-------------------------------------------------------
   |    Plugin deactivation
   |-------------------------------------------------------
   */
   public function atlt_deactivate(){

   }

}
  
$atlt=new LocoAutoTranslate();
  

