=== PW WooCommerce Bulk Edit ===
Contributors: pimwick
Donate link: https://paypal.me/pimwick
Tags: woocommerce, products, utilities, tools, bulk, batch, mass, edit, bulk edit, multiple, sale price, sale, price, pimwick
Requires at least: 4.5
Tested up to: 5.3.1
Requires PHP: 5.6
Stable tag: 2.63
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A powerful way to update your WooCommerce product catalog. Finally, no more tedious clicking through countless pages!

== Description ==

PW WooCommerce Bulk Edit is a powerful way to update your WooCommerce product catalog.

* **Live Preview** - See what you're about to change <strong>before</strong> you hit save. No more surprises!
* Inline editing in addition to bulk editing
* Safety net: you can undo changes before saving
* Edit <strong>Variations</strong> just as quickly as simple products
* Change prices by a specific amount or a percentage
* Search/replace text, append, prepend, or change capitalization
* Wildcard searches
* Keyboard navigation

> **The <a href="https://www.pimwick.com/pw-bulk-edit/">Pro version</a> includes even more great features:**
>
> * Edit so many more fields such as Categories, Sale Prices, Dates, and more! <a href="https://www.pimwick.com/pw-bulk-edit/">Click here to see the full list.</a>
> * Set or clear product images in bulk
> * Create new Variations
> * Modify selected Attributes
> * Bulk change the Sale Price based on Regular Price
> * Additional Filter options like "Is Empty" and "Is Not Empty"
> * Save and load filters
> * Use the power of Regular Expressions for searching and replacing text values
> * Support for other plugins like WooCommerce Brands and YITH Multi Vendor

**Your time is priceless**<br>
Finally, no more tedious clicking through countless pages! Make changes to your products at the same time.

**Product maintenance, evolved**<br>
Incredibly intuitive, make changes in batches or individually. Shortcut controls mimic working in a spreadsheet. Save your filters with the Pro version to make future updates a snap.

**Edit with confidence**<br>
Changes are visible and only saved when you are ready. Price drops are highlighted red for mistake-free editing.

**Relax!**<br>
You're in control of your WooCommerce product catalog with the power of PW WooCommerce Bulk Edit.

**This WooCommerce bulk editor lets you modify a variety of product fields including:**

* Product Name
* Product Description
* SKU
* Regular Price
* Tax Status
* Tax Class
* Manage Stock
* Stock Quantity
* Allow Backorders
* Stock Status
* Catalog Visibility
* Featured
* Status


== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/pw-bulk-edit` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Access the plugin using your WordPress menu under “Pimwick Plugins”

== Screenshots ==

1. Preview all changes before saving. Price drops highlight in red to give you confidence in your changes!
2. Inline editing, with keyboard navigation.
3. Complex filtering has been simplified to give you ultimate flexibility and control.
4. Powerful bulk editing for WooCommerce products.
5. Pro level features at a fraction of the price of the competition.
6. PW WooCommerce Bulk Edit is so easy to use!

== Changelog ==

= 2.63 =
* Confirmed compatibility with WooCommerce 3.9.0

= 2.62 =
* New feature: Copy a view. Integration with LiteSpeed Cache to purge the cache after saving products.

= 2.61 =
* Confirmed compatibility with the upcoming WooCommerce v3.8.0 release. Added a new wp-config option: PWBE_PREFILTER_VARIATIONS. If the data incorrectly contains product_variation records that are children of Simple Products you will want to enable this flag. It is disabled by default because typically it will not be needed and could slow down the search results.

= 2.60 =
* Added the ability to clear Number values (set to n/a) from the bulk edit menu.

= 2.59 =
* Fixed sorting by Catalog Visibility and plugin version mismatch.

= 2.58 =
* No release, plugin version mismatch.

= 2.57 =
* No release, plugin version mismatch.

= 2.56 =
* Confirmed compatibility with WooCommerce v3.6.4 and WordPress v5.2.1

= 2.55 =
* Made Variation Description part of the Standard Columns to avoid confusion.

= 2.54 =
* Updated currency bulk editor to support a higher decimal precision.

= Previous versions =
* See changelog.txt

== Upgrade Notice ==

= 2.63 =
* Confirmed compatibility with WooCommerce 3.9.0

== Frequently Asked Questions ==

**What is included with the Pro version?**

* The <a href="https://pimwick.com/pw-bulk-edit">Pro version</a> includes even more great features:
* Edit so many more fields such as Categories, Sale Prices, Dates, and more! <a href="https://pimwick.com/pw-bulk-edit">Click here to see the full list.</a>
* Create new Variations
* Modify selected Attributes
* Bulk change the Sale Price based on Regular Price
* Additional Filter options like "Is Empty" and "Is Not Empty"
* Save and load filters
* Support for other plugins like WooCommerce Brands
* <a href="https://pimwick.com/pw-bulk-edit"><strong>Learn more</strong></a>

**Where can I get the Pro version?**

* Buy the Pro Version here: <a href="https://pimwick.com/pw-bulk-edit">https://www.pimwick.com/pw-bulk-edit/</a>
