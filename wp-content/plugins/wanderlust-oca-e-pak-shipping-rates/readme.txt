=== OCA Envios para WooCommerce ===
Contributors: conradogalli 
Donate link: https://www.paypal.me/wanderlustwebdesign
Tags: woocommerce, shipping, rates, oca
Requires at least: 4.0
Tested up to: 5.2.5
Stable tag: 1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Obtener costos de envío de manera dinámica utilizando la API de OCA E-Pak.


== Description ==

Obtenga tarifas de envío con OCA dinámicamente a través de la API OCA E-Pak para sus pedidos.

Con este complemento podrá obtener automáticamente las tarifas de envío de OCA en la página del carrito y checkout con WooCommerce. Además le permite seleccionar a su cliente una sucursal de retiro. 

<strong>Acuerdo de Partners con OCA </strong>

Desde hoy accede a un 20% de descuento en tus envíos con OCA gracias al nuevo acuerdo de partners que tenemos!

Son buenas noticias, ya que vas a poder salir al mercado con mejores costos de envío!

Mas info en https://shop.wanderlust-webdesign.com/20-de-descuento-a-clientes-de-wanderlust-que-integren-con-oca/

--------*******--------

VERSION PREMIUM

Te permite generar etiquetas desde el backend de Woocommerce y obtener números de seguimiento. De esta forma evitas entrar a la página de OCA. Y asi ahorras mucho tiempo! 

Les dejo el link https://shop.wanderlust-webdesign.com/shop/woocommerce-oca-premium-shipping/ para que puedan ver todas las capturas de pantalla.

--------*******--------



== Instalacion ==

El plugin se mantiene actualizado para ser compatible con la última versión de Woocommerce y su instalación es bastante simple, consta de los siguientes pasos:

1) Instala y activa el plugin WooCommerce OCA Shipping en tu tienda.

2) Ingresá a la opción “Ajustes” del menú Woocommerce, en el panel izquierdo de tu WordPress.

3) Acede a la pestaña “Envio”, si no hay zonas de envio, generarlas.

4) En el sub menú “Zonas de envio” selecciona “Oca Express Pack”

5) Hace clic en “Activar este método de envio”

6) Completa los datos necesarios: el codigo postal de origen (SOLO NUMEROS), tu CUIT y los metodos de envio con sus respectivas operativas (estos datos se obtienen de OCA). Mas info sobre las operativas en https://shop.wanderlust-webdesign.com/que-son-las-operativas-de-oca/

7) No olvides asignarle a tus productos: Peso y dimensiones.

 
Una vez configurado con tus datos, el plugin ya está listo para calcular los costos de envío en el carrito de tu tienda.



== Planes a futuro ==

Agregar múltiples opciones de Paqueteria.



== Preguntas Frecuentes ==

Q. No aparecen costos de envio.

A. Chequear que el CUIT sea correcto, que las dimensiones y pesos esten cargados en los productos y las operativas que sean válidas.



== Changelog ==

= 1.0.6 =

* COMPATIBILIDAD CON WOO 3.8

= 1.0.5 =

* COMPATIBILIDAD CON WOO 3.7

= 1.0.4 =

* COMPATIBILIDAD CON WOO 3.5

= 1.0.3 =

* COMPATIBILIDAD CON WOO 3.4

= 1.0.2 =

* COMPATIBILIDAD CON WOO 3.3

= 1.0.1 =

* COMPATIBILIDAD CON WOO 3.2


= 1.0.0 =

* COMPATIBILIDAD CON WOO 3.0
* SELECCIONAR SUCURSAL DE DESTINO EN CHECKOUT
* SELECCIONAR SUCURSAL DE ORIGEN EN BACKEND

= 0.7.2 =

* COMPATIBILIDAD CON WOO 2.6.5

= 0.7.1 =

* COMPATIBILIDAD CON WOO 2.6

= 0.7 =

* NUMEROS CIENTIFICOS ARREGLADO

= 0.6 =

* PERMITIR SOLO NUMEROS EN CODIGO POSTAL

= 0.5 =

* ARREGLO PRODUCTOS MULTIPLES

= 0.4 =

* ARREGLO PRODUCTOS MULTIPLES
* OPCION MERCADO PAGO

= 0.3 =

* ARREGLO CALCULADORA DE ENVIO

= 0.2 =

* CUIT DESCRIPCION
* COSTOS EXTRAS

= 0.1 =

* VERSION INICIAL


== Upgrade Notice ==



== Screenshots ==

1. backend setup.
2. rates at cart.