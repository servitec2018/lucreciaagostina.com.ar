<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_shop_sidebar()
{
    return array(

        array(
            'key' => 'layout_archive_product',
            'value' => 'col-2cl'
        ),


        array(
            'key' => 'active_shop_filter',
            'value' => 'on'
        ),

        array(
            'key' => 'woocommerce_toggle_grid_list',
            'value' => 'on'
        ),

        array(
            'key' => 'woocommerce_shop_page_columns',
            'value' => array(
                'xlg' => 3,
                'lg' => 3,
                'md' => 3,
                'sm' => 2,
                'xs' => 1,
                'mb' => 1
            )
        ),

        array(
            'filter_name' => 'veera/filter/page_title',
            'value' => '<header><h1 class="page-title">Shop Left Sidebar</h1></header>'
        ),
    );
}