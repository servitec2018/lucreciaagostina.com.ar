<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_02()
{
    return array(

        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        )
    );
}