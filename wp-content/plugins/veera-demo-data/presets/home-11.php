<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_11()
{
    return array(

        array(
            'key' => 'header_layout',
            'value' => 6
        ),

        array(
            'key' => 'header_access_icon_1',
            'value' => array(
                array(
                    'type' => 'search_1',
                    'el_class' => ''
                ),
                array(
                    'type' => 'dropdown_menu',
                    'icon' => 'fa fa-user',
                    'menu_id' => 72,
                    'el_class' => ''
                ),
                array(
                    'type' => 'cart',
                    'icon' => 'dl-icon-cart1',
                    'link' => '#',
                    'el_class' => ''
                )
            )
        ),


        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.footer-top {
    border: none !important;
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        )

    );
}