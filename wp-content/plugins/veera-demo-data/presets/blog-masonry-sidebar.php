<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_blog_masonry_sidebar()
{
    return array(
        array(
            'key' => 'layout_blog',
            'value' => 'col-2cr'
        ),
        array(
            'key' => 'blog_design',
            'value' => 'grid_3'
        ),
        array(
            'key' => 'blog_masonry',
            'value' => 'on'
        ),
        array(
            'key' => 'blog_pagination_type',
            'value' => 'load_more'
        ),
        array(
            'key' => 'blog_excerpt_length',
            'value' => 20
        ),
        array(
            'key' => 'blog_thumbnail_height_mode',
            'value' => 'custom'
        ),
        array(
            'key' => 'blog_thumbnail_height_custom',
            'value' => '81%'
        ),
        array(
            'key' => 'blog_post_column',
            'value' => array(
                'xlg' => 2,
                'lg' => 2,
                'md' => 2,
                'sm' => 2,
                'xs' => 1,
                'mb' => 1
            )
        ),
        array(
            'filter_name' => 'veera/filter/page_title',
            'value' => '<header><h1 class="page-title">Blog Masonry Sidebar</h1></header>'
        ),

        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.blog-main-loop .loop__item:nth-child(8) .loop__item__thumbnail--bkg,
.blog-main-loop .loop__item:nth-child(4) .loop__item__thumbnail--bkg{
    padding-bottom: 90% !important;
}
.blog-main-loop .loop__item:nth-child(3) .loop__item__thumbnail--bkg,
.blog-main-loop .loop__item:nth-child(1) .loop__item__thumbnail--bkg{
    padding-bottom: 100% !important;
}
.blog-main-loop .entry-title {
    font-size: 18px;
}
@media (min-width: 992px){
    aside#sidebar_primary {
        width: 29%;
    }
    main#site-content {
        width: 71%;
    }
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
    );
}