<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/*
 * Template loop-end
 */
global $veera_member_loop_index, $veera_loop;
$veera_member_loop_index = '';
$loop_style = isset($veera_loop['loop_style']) ? $veera_loop['loop_style'] : 1;
?>
</div>
<!-- .team-member-loop -->
