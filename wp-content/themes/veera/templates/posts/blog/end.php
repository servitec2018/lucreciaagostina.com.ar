<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/*
 * Template loop-end
 */

$layout             = veera_get_theme_loop_prop('loop_layout', 'grid');
$style              = veera_get_theme_loop_prop('loop_style', 1);

global $veera_loop;
$veera_loop = array();
$blog_pagination_type = Veera()->settings()->get('blog_pagination_type', 'pagination');

if($layout == 'list' && $style == 1){
    echo '</div>';
}

echo '</div>';
?>
<!-- ./end-main-loop -->
<?php if($blog_pagination_type == 'load_more'): ?>
    <div class="blog-main-loop__btn-loadmore">
        <a href="javascript:;">
            <span><?php echo esc_html_x('Load more posts', 'front-view', 'veera'); ?></span>
        </a>
    </div>
<?php endif; ?>