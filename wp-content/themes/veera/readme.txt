 === Veera ===
Contributors: LA Studio
Requires at least: WordPress 5.0
Tested up to: WordPress 5.2.2, WooCommerce 3.6.5
Version: 1.1.0
Tags: one-column, two-columns, three-columns, left-sidebar, right-sidebar,  custom-background, custom-header, custom-menu, featured-images, flexible-header, full-width-template, post-formats, sticky-post, theme-options, translation-ready

== Description ==

Veera - Multipurpose WooCommerce Theme

* Mobile-first, Responsive Layout
* Custom Colors
* Custom Header
* Social Links
* Mega Menu
* Post Formats

== Installation ==

1. Navigate to Appearance → Themes in your WordPress admin dashboard.
2. Click the Add New button at the top of the page then go for the Theme Upload option.
3. For the file upload, pick Theme Files / veera.zip in the theme package downloaded from ThemeForest and click Install Now.
4. Click Activate once the upload has finished.

== Copyright ==

Copyright 2015-2019 La-StudioWeb.com
== Changelog ==

------------ Version 1.1.0 [July 19, 2019]  ------------
# Fixed swatches that conflict with the `Product Composer` plugin
^ Update version of Revolution Slider 6.0.6
^ Update version of WPBakery 6.0.5
^ Update version of LA-Studio Core 1.0.9
^ Added the option to check the latest version of the required plugins

File changes
    veera/style.css
    veera/assets/js/min/app.js
    veera/plugins/plugins.php
    veera/plugins/revslider.zip
    veera/plugins/lastudio.zip
    veera/plugins/lastudio.zip
    veera/plugins/js_composer.zip

------------ Version 1.0.9 [July 12, 2019]  ------------
# Fixed hover color for add to cart button
^ Add option to hide page title on global theme settings
^ Update version of Revolution Slider 6.0.2

File changes
    veera/assets/js/app.js
    veera/assets/js/min/app.js
    veera/framework/functions/dynamic_css.php
    veera/framework/configs/options/header.php
    veera/framework/functions/extra-functions.php
    veera/framework/configs/options/woocommerce.php
    veera/framework/configs/options/page_title_bar.php
    veera/templates/page-title-bars/layout.php
    veera/plugins/plugins.php
    veera/plugins/revslider.zip

------------ Version 1.0.8 [July 04, 2019]  ------------
* Compatibility with WooCommerce 3.6.5
# Fixed problem can't edit the page with WPBakery Page Builder 6.0.3
^ Update version of LA-Studio Core, WPBakery Page Builder plugins

File changes
    veera/style.css
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip
    veera/plugins/js_composer.zip

------------ Version 1.0.7 Release [April 19, 2019]  ------------
* Compatibility with WooCommerce 3.6.1
# Fixed shop label of breadcrumb does not work with WPML
^ Tweak Instagram Shortcode
^ Update version of LA-Studio Core plugin

File changes
    veera/style.css
    veera/woocommerce/content-product.php
    veera/woocommerce/content-single-product.php
    veera/woocommerce/global/quantity-input.php
    veera/woocommerce/loop/orderby.php
    veera/woocommerce/content-product-list-mini.php
    veera/woocommerce/single-quickview.php
    veera/framework/classes/class-breadcrumbs.php
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip
    veera/assets/js/min/app.js
    veera/assets/js/app.js

------------ Version 1.0.6 Release [March 23, 2019]  ------------
* Compatibility with WordPress 5.1.1
* Compatibility with WooCommerce 3.5.7
# Fixed a bug with the comparison function displaying incorrect custom attribute names
# Fixed a bug with the comparison function displaying incorrect stock status
^ Updated latest version of LA-Studio Core, WPBakery Visual Composer plugins
File changes
    veera/style.css
    veera/vc_templates/la_compare.php
    veera/vc_templates/la_wishlist.php
    veera/framework/functions/functions.php
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip
    veera/plugins/revslider.zip
    veera/plugins/js_composer.zip

------------ Version 1.0.5 Release [ December 08, 2018]  ------------
* Compatibility with WordPress 5.0
* Compatibility with WooCommerce 3.5.2

^ Updated latest version of LA-Studio Core, WPBakery Visual Composer plugins

File changes
    veera/style.css
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip
    veera/plugins/js_composer.zip

------------ Version 1.0.4 Release [November 27, 2018]  ------------
* Compatibility with WooCommerce 3.5.1
# Fixed Position of "comment count" and "favorite count" display incorrect in Firefox and IE 10.
# Fixed product category display incorrect when filtered
^ Updated latest version of WPBakery Visual Composer plugin
File changes
    veera/assets/js/app.js
    veera/assets/js/min/app.js
    veera/woocommerce/single-product/product-image.php
    veera/framework/functions/extra-functions.php
    veera/style.css
    veera/plugins/plugins.php
    veera/plugins/js_composer.zip

------------ Version 1.0.3.1 Release [August 14, 2018]  ------------
# Fixed value can not be change on Customize mode
# Fixed Animation Block has been delayed when viewed on mobile

File changes
    veera/assets/js/app.js
    veera/assets/js/min/app.js
    veera/framework/classes/class-setting.php
    veera/framework/classes/class-scripts.php

------------ Version 1.0.3 Release [August 13, 2018]  ------------
# Fixed Newsletter Popup does not display on mobile
^ Fixed WooCommerce Product CSV Export & Imported problem
^ Update version of LA-Studio Core plugin
File changes
    veera/woocommerce/product-searchform.php
    veera/assets/js/app.js
    veera/assets/js/min/app.js
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip

------------ Version 1.0.2 Release [August 10, 2018]  ------------
# Fixed problem the filter does not works when viewed on the product category page
# Fixed problem newsletter popup does not display on mobile
# Fixed problem can't add to cart on product page ( if you activate the booking plugin )
# Added style for out of stock badge
^ Update version of LA-Studio Core plugin

File changes
    veera/style.css
    veera/assets/js/min/app.js
    veera/assets/js/app.js
    veera/plugins/plugins.php
    veera/plugins/lastudio.zip
    veera/framework/classes/class-helper.php
    veera/framework/classes/class-woocommerce.php
    veera/templates/footers/footer-bottom.php

------------ Version 1.0.1 Release [July 20, 2018]  ------------
^ Fixed blog infinite scroll problem
^ Added `Header Layout 11`
^ Allow setup the width for Logo
^ Added Popup Newsletter Options

File changes
    veera/framework/configs/options/header.php
    veera/templates/headers/header-11.php
    veera/framework/configs/options/general.php
    veera/framework/functions/additional_css.php
    veera/templates/posts/blog/start.php
    veera/templates/posts/loop.php
    veera/templates/footers/footer-bottom.php
    veera/style.css

------------ Version 1.0 Release [July 20, 2018]  ------------
* Release

Initial release