<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action( 'tgmpa_register', 'veera_register_required_plugins' );

if(!function_exists('veera_register_required_plugins')){

	function veera_register_required_plugins() {

        $initial_required = array(
            'lastudio' => array(
                'source'    => 'https://la-studioweb.com/file-resouces/veera/plugins/lastudio/1.0.9/lastudio.zip',
                'version'   => '1.0.9'
            ),
            'veera-demo-data' => array(
                'source'    => 'https://la-studioweb.com/file-resouces/veera/plugins/veera-demo-data/1.0.1/veera-demo-data.zip',
                'version'   => '1.0.1'
            ),
            'revslider' => array(
                'source'    => 'https://la-studioweb.com/file-resouces/shared/plugins/revslider/6.0.6/revslider.zip',
                'version'   => '6.0.6'
            ),
            'js_composer' => array(
                'source'    => 'https://la-studioweb.com/file-resouces/shared/plugins/js_composer/6.0.5/js_composer.zip',
                'version'   => '6.0.5'
            )
        );

        $from_option = get_option('veera_required_plugins_list', $initial_required);

		$plugins = array();

		$plugins['js_composer'] = array(
			'name'					=> esc_html_x('WPBakery Visual Composer', 'admin-view', 'veera'),
			'slug'					=> 'js_composer',
            'source'				=> isset($from_option['js_composer'], $from_option['js_composer']['source']) ? $from_option['js_composer']['source'] : $initial_required['js_composer']['source'],
			'required'				=> true,
            'version'				=> isset($from_option['js_composer'], $from_option['js_composer']['version']) ? $from_option['js_composer']['version'] : $initial_required['js_composer']['version']
		);

		$plugins['lastudio'] = array(
			'name'					=> esc_html_x('LA-Studio Core', 'admin-view', 'veera'),
			'slug'					=> 'lastudio',
            'source'				=> isset($from_option['lastudio'], $from_option['lastudio']['source']) ? $from_option['lastudio']['source'] : $initial_required['lastudio']['source'],
			'required'				=> true,
            'version'				=> isset($from_option['lastudio'], $from_option['lastudio']['version']) ? $from_option['lastudio']['version'] : $initial_required['lastudio']['version']
		);

		$plugins['woocommerce'] = array(
			'name'     				=> esc_html_x('WooCommerce', 'admin-view', 'veera'),
			'slug'     				=> 'woocommerce',
			'version'				=> '3.6.5',
			'required' 				=> false
		);

		$plugins['envato-market'] = array(
			'name'     				=> esc_html_x('Envato Market', 'admin-view', 'veera'),
			'slug'     				=> 'envato-market',
			'source'   				=> 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
			'required' 				=> false,
			'version' 				=> '2.0.1'
		);

		$plugins['veera-demo-data'] = array(
			'name'					=> esc_html_x('Veera Package Demo Data', 'admin-view', 'veera'),
			'slug'					=> 'veera-demo-data',
            'source'				=> isset($from_option['veera-demo-data'], $from_option['veera-demo-data']['source']) ? $from_option['veera-demo-data']['source'] : $initial_required['veera-demo-data']['source'],
			'required'				=> true,
            'version'				=> isset($from_option['veera-demo-data'], $from_option['veera-demo-data']['version']) ? $from_option['veera-demo-data']['version'] : $initial_required['veera-demo-data']['version']
		);

		$plugins['contact-form-7'] = array(
			'name' 					=> esc_html_x('Contact Form 7', 'admin-view', 'veera'),
			'slug' 					=> 'contact-form-7',
			'required' 				=> false
		);

		$plugins['revslider'] = array(
			'name'					=> esc_html_x('Slider Revolution', 'admin-view', 'veera'),
            'slug'					=> 'revslider',
            'source'				=> isset($from_option['revslider'], $from_option['revslider']['source']) ? $from_option['revslider']['source'] : $initial_required['revslider']['source'],
            'required'				=> false,
            'version'				=> isset($from_option['revslider'], $from_option['revslider']['version']) ? $from_option['revslider']['version'] : $initial_required['revslider']['version']
		);

		$config = array(
			'id'           				=> 'veera',
			'default_path' 				=> '',
			'menu'         				=> 'tgmpa-install-plugins',
			'has_notices'  				=> true,
			'dismissable'  				=> true,
			'dismiss_msg'  				=> '',
			'is_automatic' 				=> false,
			'message'      				=> ''
		);

		tgmpa( $plugins, $config );

	}

}
